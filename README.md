The datapackage shows the daily meat consumption per person, per country in 2002 in 2009

## Data
The data is sourced from: 
* https://en.wikipedia.org/wiki/List_of_countries_by_meat_consumption

The repo includes:
* datapackage.json
* meatconsumption.csv
* process.py

--------------------------------------------------------------------------------------------------------------------------------

## Preparation

Requires:
1. Python
2. Visualstudio / Jupyter notebook / or any platform that works with python.

***meatconsumption.csv*** 
* The CSV Data has 3 colummns. 
    * Country
    * Kg/person (2002)
    * Kg/person (2009)

***datapackage.json***
* The json file has all the information from the two csv files, licence, author and includes 1 line graphs.

***process.py***
* The script will scrape the whole table from - https://en.wikipedia.org/wiki/List_of_countries_by_meat_consumption
    
***Instructions:***
* Copy process.py script in the "Process" folder.
* Open in jupyter notebook, python shell, VS code, or any preferred platform.
* Run the code
* 1 CSV file will be saved in document where your terminal is at the moment.
----------------------------------------------------------------------------------------------------------------------------------------

## Licence
This Data Package is made available under the Public Domain Dedication and License v1.0 