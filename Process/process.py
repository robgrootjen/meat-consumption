import requests
from bs4 import BeautifulSoup
import csv

#################################################################################################

#Get content
result1 = requests.get('https://en.wikipedia.org/wiki/List_of_countries_by_meat_consumption')

#####################################################################################################

#Save source in variable
src1 = result1.content

######################################################################################################

#Activate soup
soup = BeautifulSoup(src1,'lxml')

###################################################################################################

#Look for table and save in csv
table = soup.find('table')
with open('meatconsumption.csv','w',newline='') as f:
    writer = csv.writer(f)
    for tr in table('tr'):
        row = [t.get_text(strip=True) for t in tr (['td','th'])]
        cell1 = row[0]
        cell2 = row[1]
        cell3 = row[2].replace('[10]','').replace('[11]','')
        row = [cell1,cell2,cell3]
        writer.writerow(row)

##################################################################################################